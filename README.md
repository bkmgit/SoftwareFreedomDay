
Uks päev linuxi õppikoda 

* Arvuti ohutusga [Fedora](https://getfedora.org/), 
[Debian](https://www.debian.org/), [PureOs](https://puri.sm/pureos/) ja 
[Qubes](https://www.qubes-os.org/) 
* Inimese arvuti ehitus ja installeremine avatud tarkvara. Võimalik  
hinnaalandlus arvuti koostist [Ordi](http://www.ordi.ee/), 
[Markit](https://www.markit.eu/ee/en/Catalog.aspx) ...
* Organiseerimine  [IT college](http://www.itcollege.ee), 
[Tartu Ülikool](http://cs.ut.ee), [Pingviin](https://pingviin.org/), 
[Alvatal](http://alvatal.ee/)
* Kokkuvöte
  1. Tuvustamine 9-10:30
  2. Kohvi paus 10:30-11:00
  3. Arvuti ehitimine ja avatud tarkvara paigaldamine 11:00-13:00
  4. lõunaaeg 13:00-14:00
  5. Tarkvara tõestamine (inkscape, gimp, SELinux, virtualisaation ...) 14:00-15:00
  6. Avatud tarkvara ehitimine (dokumenteerima, tunnus lisa ...) 15:00-16:00
  7. Kohvi paus 16:00-16:30
  8. Fedora, Qubes ja eesti ID kard 16:30-17:00
